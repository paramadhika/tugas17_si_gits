<?php

require_once "connection.php";

    if(function_exists($_GET['function'])){
        $_GET['function']();
    }

    function pushNotification(){

        $url = 'https://fcm.googleapis.com/fcm/send';

        $check = array(
            'registration_id' => '',
            'message' => '',
            'title' => ''
        );
    
        $headers = array(
            'Authorization: key=' . FIREBASE_SERVER_KEY,
            'Content-Type: application/json'
        );

        $checkDataInput = count(array_intersect_key($_POST, $check));
        if($checkDataInput == count($check)){
            $token = $_POST['registration_id'];
            $message['title'] = $_POST['title'];
            $message['message'] = $_POST['message'];

            $fields = array(
                'to' => '/topics/' . $token,
                'data' => $message
            );

            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    

            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }

            curl_close($ch);
            
            if(empty($result)){
                $response = array (
                    'status'=> 204,
                    'message' => 'Gagal Mengirim Notifikasi!',
                    'data' => $result
                    );
                }
            else{
                $response = array (
                    'status'=> 200,
                    'message' => 'Berhasil mengirim Notifikasi!'
                    );
                }
        }
        else{
            $response = array (
                'status'=> 400,
                'message' => 'Parameter tidak sesuai!',
            );
        }
        echo json_encode($response);
        return $result;
    }
?>
