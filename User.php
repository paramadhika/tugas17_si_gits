<?php
require_once "connection.php";

if(function_exists($_GET['function'])){
    $_GET['function']();
}

function Login(){
    global $connect;

    $check = array(
        "email" => "",
        "password" => ""
    );

    $checkDataInput = count(array_intersect_key($_POST, $check));
    if($checkDataInput == count($check)){
        $email = $_POST['email'];
        $pass = $_POST['password'];
        $query = mysqli_query($connect, "SELECT * FROM useridentity 
                                         WHERE email = '$email' AND password = '$pass'");

        $check_user_data = mysqli_num_rows($query);

        if($check_user_data > 0){
            while($row = mysqli_fetch_object($query)){
                $result[] = $row;   
            }
        }
        else{
            $response = array (
                'status'=> 204,
                'message' => 'Tidak Ada Data Ditemukan!'
            );
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Login Gagal!',
                'data' => $result
                );
            }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Login Berhasil!',
                'data' => $result
                );
            }
        echo json_encode($response);
    }

}

function ListUser(){
    global $connect;

    $query = mysqli_query($connect, "SELECT * FROM useridentity");

    while($row = mysqli_fetch_object($query)){
        $result[] = $row;
    }

    if(empty($result)){
        $response = array (
            'status'=> 204,
            'message' => 'Data tidak ditemukan!',
            'data' => $result
            );
        }
    else{
        $response = array (
            'status'=> 200,
            'message' => 'Berhasil mengambil data!',
            'data' => $result
            );
        }
    echo json_encode($response);
}

function UserbyId(){
    global $connect;

        $id = $_GET['id'];
    
        $query = mysqli_query($connect, "SELECT * FROM useridentity WHERE id = $id");

        while($row = mysqli_fetch_object($query)){
            $result = $row;
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Data tidak ditemukan!',
                'data' => $result
            );
        }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Berhasil mengambil data!',
                'data' => $result
            );
        }
    echo json_encode($response);
}

function daftarUserBaru(){
    global $connect;

    $checkTable = array(
        'firstname' => '',
        'lastname' => '',
        'email' => '',
        'password' => ''
    );

    $checkDataInput = count(array_intersect_key($_POST, $checkTable));

    if($checkDataInput == count($checkTable)){
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $pass = $_POST['password'];
        
        $result = mysqli_query($connect, "INSERT INTO useridentity(firstname, lastname, email, `password`) 
                                VALUE ('$firstname', '$lastname', '$email', '$pass')");
        
        if($result){
            $response = array (
                'status'=> 201,
                'message' => 'Data berhasil ditambahkan!',
            );
        }
        else{
            $response = array (
                'status'=> 401,
                'message' => 'Data tidak berhasil ditambahkan!',
            );
        }
    }
    else{
        $response = array (
            'status'=> 400,
            'message' => 'Parameter tidak sesuai!',
        );
    }

    echo json_encode($response);
}

function updateUser(){
    global $connect;

    if(!empty($_GET['id'])){
        $id = $_GET['id'];
    
        $checkTable = array(
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'password' => '',
            'image' => '',
        );

        $checkDataInput = count(array_intersect_key($_POST, $checkTable));

        if($checkDataInput == count($checkTable)){
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $email = $_POST['email'];
            $pass = $_POST['password'];
            $image = $_POST['image'];

            $result = mysqli_query($connect, "UPDATE useridentity SET 
                                    firstname = '$firstname', lastname = '$lastname',
                                    email = '$email', `password` = '$pass', image = '$image' WHERE id = $id");

            if($result){
                $response = array (
                    'status'=> 201,
                    'message' => 'Data berhasil di Update!',
                );
            }
            else{
                $response = array (
                    'status'=> 401,
                    'message' => 'Data tidak berhasil di Update!',
                );
            }   
        }
        else{
            $response = array (
                'status'=> 401,
                'message' => 'Parameter tidak sesuai!',
            );
        }
    }
    else{
        $response = array (
            'status'=> 401,
            'message' => 'Data tidak ada!',
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function deleteUser(){
    global $connect;
    
    $id = $_GET['id'];
    $result = mysqli_query($connect, "DELETE FROM useridentity WHERE id = $id");

    if($result){
        $response = array (
            'status'=> 201,
            'message' => 'Data berhasil dihapus!',
        );
    }
    else{
        $response = array (
            'status'=> 401,
            'message' => 'Error!',
        );
    }
    echo json_encode($response);
}

function user_image(){
    global $connect;
    
    $image = $_FILES['file']['tmp_name'];
    $imageUser = $_FILES['file']['name'];

    $imgPath = $_SERVER['DOCUMENT_ROOT'].'/todolistAPI/ImageUpload';
 
    if(!file_exists($imgPath)){
        mkdir($imgPath, 0777, true);
    }

    if(!$image){
        $response = array (
            'status'=> 400,
            'message' => 'Gambar Tidak Ditemukan!',
        );
    }
    else{
        if(move_uploaded_file($image, $imgPath.'/'.$imageUser)){
            $response = array (
                'status'=> 200,
                'message' => 'Gambar Sukses di Upload!',
            );
        }
    }
    echo json_encode($response);
}

function login_auth(){
    global $connect;

    $device_id = $_POST['device_id'] ?: '';

    if($device_id != null && !empty($device_id)){

        $query = mysqli_query($connect, "SELECT * FROM useridentity WHERE device_id = '$device_id'");

        while($row = mysqli_fetch_object($query)){
            $result[] = $row;
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Gagal Login!',
                'data' => $result
            );
        }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Berhasil Login!',
                'data' => $result
            );
        }

    }
    else{
        $response = array (
            'status'=> 200,
            'message' => 'Device ID Not Registered!'
        );
    }
    echo json_encode($response);
}

function update_deviceId(){
    global $connect;

    $device_id = $_POST['device_id'] ?: '';
    $id = $_POST['id'] ?: '';

    if(!empty($id)){

        $result = mysqli_query($connect, "UPDATE useridentity SET device_id = '$device_id' WHERE id = $id");

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Gagal memperbarui ID Device!'
            );
        }
        else{
            if(!empty($device_id))
                $response = array (
                    'status'=> 200,
                    'message' => 'Success Bind Account!'
                );
            else{
                $response = array (
                    'status'=> 200,
                    'message' => 'Success Unbind Account!'
                );
            }
        }
    }
    else{
        $response = array (
            'status'=> 204,
            'message' => 'Device ID or Account Not Found!'
        );
    }
    echo json_encode($response);
}

?>