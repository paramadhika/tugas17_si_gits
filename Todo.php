<?php
    require_once "connection.php";

    if(function_exists($_GET['function'])){
        $_GET['function']();
    }

    function getTodoList(){
        global $connect;

        $query = mysqli_query($connect, "SELECT * FROM todolist");
    

        while($row = mysqli_fetch_object($query)){
            $result[] = $row;
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Data tidak ada!',
                'data' => $result
            );
        }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Berhasil mengambil data!',
                'data' => $result
            );
        }
        echo json_encode($response);
    }

    function TodoListbyId(){
        global $connect;

        if(!empty($_GET['id'])){
            $id = $_GET['id'];
        
            $query = mysqli_query($connect, "SELECT * FROM todolist WHERE id = $id");

            while($row = mysqli_fetch_object($query)){
                $result = $row;
            }
    
            if(empty($result)){
                $response = array (
                    'status'=> 204,
                    'message' => 'Data tidak ditemukan!',
                    'data' => $result
                );
            }
            else{
                $response = array (
                    'status'=> 200,
                    'message' => 'Berhasil mengambil data!',
                    'data' => $result
                );
            }
        }
        else{
            $response = array (
                'status'=> 400,
                'message' => 'Data Tidak tersedia!',
            );
        }

        echo json_encode($response);
    }

    function tambahTodo(){
        global $connect;

        $checkTable = array(
            'todo' => ''
        );

        $checkDataInput = count(array_intersect_key($_POST, $checkTable));

        if($checkDataInput == count($checkTable)){
            $todo = $_POST['todo'];
            $todostatus = 0;
            $date_finished = "0000-00-00 00:00:00";
            
            $result = mysqli_query($connect, "INSERT INTO todolist(todo, todoStatus, date_created, date_finished) 
                                    VALUE ('$todo', '$todostatus',now(), '$date_finished')");
            
            if($result){
                $response = array (
                    'status'=> 201,
                    'message' => 'Data berhasil ditambahkan!',
                );
            }
            else{
                $response = array (
                    'status'=> 401,
                    'message' => 'Data tidak berhasil ditambahkan!',
                );
            }
        }
        else{
            $response = array (
                'status'=> 400,
                'message' => 'Parameter tidak sesuai!',
            );
        }

        echo json_encode($response);
    }

    function updateTodo(){
        global $connect;

        if(!empty($_GET['id'])){
            $id = $_GET['id'];
        
            $checkTable = array(
                'todo' => '',
                'todoStatus' => '' 
            );
            

            $checkDataInput = count(array_intersect_key($_POST, $checkTable));

            if($checkDataInput == count($checkTable)){
                if(isset($_POST['todo']) && isset($_POST['todoStatus'])){
                    $todo = $_POST['todo'];
                    $todoStatus = $_POST['todoStatus'];
                    if($todoStatus == 1 ){
                        $date_finished = "now()";
                    }
                    else{
                        $date_finished = "'0000-00-00 00:00:00'";
                    }
                }        
                else{
                    $response = array (
                        'status'=> 401,
                        'message' => 'Silahkan Masukkan Data!',
                    );
                }

                $result[] = mysqli_query($connect, "UPDATE todolist SET todo = '$todo', todoStatus = $todoStatus, date_finished = $date_finished WHERE id = $id");
                
                if($result){
                    $response = array (
                        'status'=> 201,
                        'message' => 'Data berhasil di Update!',
                    );
                }
                else{
                    $response = array (
                        'status'=> 401,
                        'message' => 'Data tidak berhasil di update!',
                    );
                }   
            }
            else{
                $response = array (
                    'status'=> 401,
                    'message' => 'Parameter tidak sesuai!',
                );
            }
        }
        else{
            $response = array (
                'status'=> 401,
                'message' => 'Data tidak ada!',
            );
        }
        echo json_encode($response);
    }

    function deleteTodo(){
        global $connect;
        
        $id = $_GET['id'];
        $result = mysqli_query($connect, "DELETE FROM todolist WHERE id = $id");

        if($result){
            $response = array (
                'status'=> 201,
                'message' => 'Data berhasil dihapus!',
            );
        }
        else{
            $response = array (
                'status'=> 401,
                'message' => 'Error!',
            );
        }
        echo json_encode($response);
    }

    function getCompleted(){
        global $connect;

        $query = mysqli_query($connect, "SELECT * FROM todolist WHERE todoStatus = 1");

        $check_todo_data = mysqli_num_rows($query);

        if($check_todo_data > 0){
            while($row = mysqli_fetch_object($query)){
                $result[] = $row;   
            }
        }
        else{
            $response = array (
                'status'=> 204,
                'message' => 'Tidak Ada Data Ditemukan!'
            );
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Data tidak ada!',
                'data' => $result
            );
            $result[] = 0;
        }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Berhasil mengambil data!',
                'data' => $result
            );
        }
        echo json_encode($response);
    }

    function getUncompleted(){
        global $connect;

        $query = mysqli_query($connect, "SELECT * FROM todolist WHERE todoStatus = 0");

        $check_todo_data = mysqli_num_rows($query);

        if($check_todo_data > 0){
            while($row = mysqli_fetch_object($query)){
                $result[] = $row;   
            }
        }
        else{
            $response = array (
                'status'=> 204,
                'message' => 'Tidak Ada Data Ditemukan!'
            );
            $result[] = 0;
        }

        if(empty($result)){
            $response = array (
                'status'=> 204,
                'message' => 'Data tidak ada!',
                'data' => $result
            );
        }
        else{
            $response = array (
                'status'=> 200,
                'message' => 'Berhasil mengambil data!',
                'data' => $result
            );
        }
        echo json_encode($response);
    }
?>